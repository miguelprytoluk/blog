class Comment < ActiveRecord::Base
  belongs_to :post
  validates :post_id, numericality: {greater_than_or_equal_to: 1, only_integer: true }, presence: true
  validates_presence_of :body
end
